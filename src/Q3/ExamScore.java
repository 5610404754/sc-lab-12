package Q3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ExamScore {

	String filename = "average.txt";
	FileReader fileReader = null;

	String filename2 = "exam.txt";
	FileReader fileReader2 = null;
	FileWriter writer = null;
	int half;

	ArrayList<String> name = new ArrayList<String>();
	ArrayList<Double> averageList = new ArrayList<Double>();
	private FileReader fileReader3;

	public void examAvg() {
		try {
			fileReader2 = new FileReader(filename2);
			BufferedReader buffer2 = new BufferedReader(fileReader2);
			String line2;
			for (line2 = buffer2.readLine(); line2 != null; line2 = buffer2
					.readLine()) {
				String[] sep = line2.split(",");
				name.add(sep[0]);
				Double avg = (Double.parseDouble(sep[1]) + Double
						.parseDouble(sep[2])) / 2;
				averageList.add(avg);
			}

		} catch (FileNotFoundException e) {
			System.err.println("Cannot read file " + filename);

		} catch (IOException e) {
			System.err.println("Error reading from file");

		}

	}

	public void HomeWorkScore() {
		try {

			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			int nm = 0;
			System.out.println("--------- Homework Scores ---------");
			System.out.println(" Name Average");
			System.out.println(" ==== =======");
			for (line = buffer.readLine(); line != null; line = buffer
					.readLine()) {
				if (nm == half) {
					System.out.println("--------- Exam Scores ---------");
					System.out.println(" Name Average");
					System.out.println(" ==== =======");
					System.out.println(" " + line);
				} else {
					System.out.println(" "+ line);

				}
				nm += 1;
			}

		} catch (FileNotFoundException e) {
			System.err.println("Cannot read file " + filename);

		} catch (IOException e) {
			System.err.println("Error reading from file");

		}
	}

	public void writeExamAvg() {
		try {

			writer = new FileWriter("average.txt", true);
			BufferedWriter out = new BufferedWriter(writer);

			for (int num = 0; num < name.size(); num++) {
				String allword = name.get(num) + ", "
						+ averageList.get(num).toString() + "\n";
				out.write(allword);

			}

			out.flush();
		} catch (FileNotFoundException e) {
			System.err.println("Cannot read file " + filename);

		} catch (IOException e) {
			System.err.println("Error reading from file");

		}
	}

	public String toString(int r) {
		return "" + averageList.get(r);
	}
	public void readNumLine(){
		try{
		fileReader = new FileReader(filename);
		BufferedReader buffer = new BufferedReader(fileReader);
		String line3;
		int count = 0;
		for (line3 = buffer.readLine(); line3 != null; line3 = buffer
				.readLine()) {
				count += 1;
			half = count / 2;
			
			
		}

	} catch (FileNotFoundException e) {
		System.err.println("Cannot read file " + filename);

	} catch (IOException e) {
		System.err.println("Error reading from file");

	}
	}
}
