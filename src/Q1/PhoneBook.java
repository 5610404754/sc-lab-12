package Q1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBook {
	String filename = "phonebook.txt";
	FileReader fileReader = null;
	ArrayList<ListBook> list = new ArrayList<ListBook>();

	public void running() {

		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			int n = 1;
			System.out.println("--------- Java Phone Book ---------"+"\n");
			System.out.println("     "+"Name"+"  "+  "Phone"+"\n");
			System.out.println("    "+" ==== "+" "+"========= "+"\n");

			for (line = buffer.readLine(); line != null; line = buffer
					.readLine()) {
				System.out.println("No." + n +" "+ line);
				String[] sep = line.split(",");
				list.add(new ListBook(sep[0], sep[1]));
				n += 1;
			}


		} catch (FileNotFoundException e) {
			System.err.println("Cannot read file " + filename);

		} catch (IOException e) {
			System.err.println("Error reading from file");

		}

	}

}
