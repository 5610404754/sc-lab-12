package Q2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Q1.ListBook;

public class HomeWork {

	String filename = "homework.txt";
	String filename2 = "average.txt";

	FileReader fileReader = null;
	FileReader fileReader2 = null;
	FileWriter writer = null;
	ArrayList<Double> averageList = new ArrayList<Double>();
	ArrayList<String> name = new ArrayList<String>();


	public void readWork(){
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;									

			for (line = buffer.readLine(); line != null; line = buffer
					.readLine()) {
				String[] sep = line.split(",");
				name.add(sep[0]);
				double total = 0;
				for(int i=1;i<sep.length;i++){
					total += Double.parseDouble(sep[i]);
				}
				double allavg = average(total,sep.length-1);
				averageList.add(allavg);			
			}
			
			

		} catch (FileNotFoundException e) {
			System.err.println("Cannot read file " + filename);

		} catch (IOException e) {
			System.err.println("Error reading from file");

		}
	}
	public void writeScore(){
		try {
			
			writer = new FileWriter("average.txt");
			BufferedWriter out = new BufferedWriter(writer);
			for(int n=0;n<name.size();n++){
				String allword = name.get(n)+", "+averageList.get(n).toString()+"\n";
				out.write(allword);
			}
			out.flush();

			
		}catch(FileNotFoundException e){
			System.err.println("File not found");
		} catch (IOException e) {
			System.err.println("Cannot read File from User");

		}
		
	}
	public void readAvgScore(){
try {
			
			System.out.println("--------- Homework Scores ---------");
			System.out.println("\t"+"Name  Average");
			System.out.println("\t"+"====  =======");
			fileReader2 = new FileReader(filename2);
			BufferedReader buffer2 = new BufferedReader(fileReader2);
			String line2;
			int mn = 1;
			for (line2 = buffer2.readLine(); line2 != null; line2 = buffer2
					.readLine()) {
				System.out.println("No."+mn+"    "+ line2);	
				mn++;
			}
			
		}catch(FileNotFoundException e){
			System.err.println("File not found");
		} catch (IOException e) {
			System.err.println("Cannot read File from User");

		}					
		
	}
	public double average(double num, double amount){		
		double all = num/amount;		
		return all;
	}
	public String toString(int r){
		return "" + averageList.get(r);
	}
}
